class MainCompassModel {
  static final List<CompassBean> compassBeanList = <CompassBean>[
    CompassBean(
      name: "Free Fayer",
      image: "icon_free_fayer.png",
      players: "18.8k",
    ),
    CompassBean(
      name: "Clash Royale",
      image: "icon_cr.png",
      players: "28.6k",
    ),
    CompassBean(
      name: "League of leguends",
      image: "iocn_lol.png",
      players: "28.6k",
    ),
  ];

  static final List<String> todasList = <String>[
    "icon_maincra.png",
    "icon_cod.png",
    "icon_fail.png",
    "icon_pc.png",
  ];
}

class CompassBean {
  CompassBean({this.name, this.image, this.players});

  String? name;
  String? image;
  String? players;
}
