import 'dart:math';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:special_test/common/widget/background_widget.dart';
import 'package:special_test/common/widget/user_information_widget.dart';
import 'package:special_test/common/theme/theme_color.dart';
import 'package:special_test/mainCompass/model/main_compass_model.dart';
import 'package:special_test/mainCompass/view/widget/main_compass_clip_widget.dart';

class MainCompassView extends StatefulWidget {
  const MainCompassView({Key? key}) : super(key: key);

  @override
  _MainCompassViewState createState() => _MainCompassViewState();
}

class _MainCompassViewState extends State<MainCompassView> {
  @override
  Widget build(BuildContext context) => Scaffold(
        body: BackgroundWidget(
          child: Stack(
            children: <Widget>[
              const UserInformationWidget(),
              Positioned(
                top: 130,
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  padding: const EdgeInsets.all(20),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        buildCompassTextRow(),
                        const SizedBox(height: 20),
                        buildCompassListContainer(),
                        const SizedBox(height: 30),
                        buildCategoriasTextRow(),
                        const SizedBox(height: 20),
                        buildTodasText(),
                        const SizedBox(height: 20),
                        buildTodasListColumn(),
                        const SizedBox(height: 80),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  Column buildTodasListColumn() {
    return Column(
      children: List<Widget>.generate(
        MainCompassModel.todasList.length,
        (index) => Container(
          height: 120,
          alignment: Alignment.bottomCenter,
          margin: const EdgeInsets.only(bottom: 5),
          child: Stack(
            children: <Widget>[
              Positioned(
                bottom: 0,
                left: 5,
                right: 0,
                child: Container(
                  alignment: Alignment.bottomCenter,
                  padding: const EdgeInsets.only(left: 100, top: 10, right: 20),
                  height: 90,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const <Widget>[
                      Text(
                        "Fall Guys: Ultimate Knockout",
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        "500k espectadores - 4k siguiendo",
                      )
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 0,
                left: 0,
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      border: Border.all(
                          color: ThemeColor.primary.withOpacity(0.3),
                          width: 8)),
                  child: Image.asset(
                    "assets/images/${MainCompassModel.todasList[index]}",
                    width: 100,
                    height: 100,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Text buildTodasText() {
    return const Text("Todas las categorias",
        style: TextStyle(
            fontSize: 14, color: Colors.white, fontWeight: FontWeight.w500));
  }

  Row buildCategoriasTextRow() {
    return Row(
      children: <Widget>[
        Column(
          children: <Widget>[
            const Text("Categorias",
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.w500)),
            const SizedBox(
              height: 3,
            ),
            Container(
                width: 70,
                height: 3,
                decoration: const BoxDecoration(
                    color: Color(0xff6630DE),
                    boxShadow: <BoxShadow>[
                      BoxShadow(blurRadius: 5, color: Color(0xA68E63EB))
                    ]))
          ],
        ),
        const Padding(
            padding: EdgeInsets.only(left: 20, bottom: 5),
            child: Text("Canales en vivo",
                style: TextStyle(fontSize: 14, color: Colors.grey))),
      ],
    );
  }

  Container buildCompassListContainer() {
    return Container(
      height: 300,
      alignment: Alignment.bottomCenter,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: MainCompassModel.compassBeanList.length,
        itemBuilder: (_, index) => Stack(
          children: <Widget>[
            Container(
              alignment: Alignment.bottomCenter,
              margin: const EdgeInsets.only(right: 20),
              child: MainCompassClipWidget(
                positon: CPositon.TOP,
                width: 180,
                height: 250,
                mirror: (index % 2) == 1,
                colorBorder: Colors.white.withOpacity(0.25),
                widget: Transform(
                  alignment: Alignment.center,
                  transform: Matrix4.rotationY((index % 2) == 1 ? pi : 0),
                  child: ClipPath(
                    clipper: RoundedDiagonalPathClipper(),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                      child: Container(
                        width: 180,
                        height: 280,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(25),
                          ),
                          color: Colors.white.withOpacity(0.08),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Image.asset(
                "assets/images/${MainCompassModel.compassBeanList[index].image}",
                width: 250,
                height: 300,
                fit: BoxFit.cover,
              ),
            ),
            buildCompassListNamePositioned(index)
          ],
        ),
      ),
    );
  }

  Positioned buildCompassListNamePositioned(int index) {
    return Positioned(
      bottom: 20,
      left: 20,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(MainCompassModel.compassBeanList[index].name!,
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold)),
            Text(MainCompassModel.compassBeanList[index].players!,
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ]),
    );
  }

  Row buildCompassTextRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
                width: 10,
                height: 10,
                margin: const EdgeInsets.only(right: 10),
                decoration: const BoxDecoration(
                    shape: BoxShape.circle, color: Color(0xff6630DE))),
            const Text("Compass",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                    fontWeight: FontWeight.bold))
          ],
        ),
        const Text("Top Games",
            style: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.w700)),
      ],
    );
  }
}
