import 'package:flutter/material.dart';
import 'package:special_test/introduction/view/introduction_screen.dart';
import 'package:special_test/common/util/logger_util.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    log.d('_SplashScreenState, initState');
    Future<void>.delayed(const Duration(seconds: 2), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute<dynamic>(
          builder: (_) => const IntroductionScreen(),
        ),
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Container(
            color: const Color(0xFF72A2C0),
            padding: const EdgeInsets.all(32),
            child: Center(child: Image.asset("assets/images/icon_logo.png"))),
      );
}
