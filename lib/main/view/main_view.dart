import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:special_test/common/widget/background_widget.dart';
import 'package:special_test/common/util/logger_util.dart';
import 'package:special_test/main/update/main_cubit.dart';
import 'package:special_test/main/view/widget/main_bottom_bar_widget.dart';
import 'package:special_test/mainCompass/view/main_compass_view.dart';
import 'package:special_test/mainHome/view/main_home_view.dart';
import 'package:special_test/main/view/widget/main_hexagonal_clipper_widget.dart';

class MainView extends StatefulWidget {
  const MainView({Key? key}) : super(key: key);

  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  int page = 0;
  int category = 0;

  @override
  Widget build(BuildContext context) {
    log.d('_MainViewState, build');
    return BlocBuilder<MainCubit, int>(
      builder: (context, state) => Scaffold(
        body: BackgroundWidget(
          child: Stack(
            children: <Widget>[
              buildPageWidget(state),
              buildTetheringPositioned(),
              Positioned(
                  bottom: -20,
                  child: MainBottomBarWidget(
                      page: state,
                      onSelect: (index) =>
                          context.read<MainCubit>().changedPage(index))),
            ],
          ),
        ),
      ),
    );
  }

  Positioned buildTetheringPositioned() {
    return Positioned.fill(
      bottom: 80,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: ClipPath(
          clipper: MainHexagonalClipperWidget(),
          child: Container(
            height: 60,
            width: 75,
            padding: const EdgeInsets.only(bottom: 10),
            color: const Color(0xff6630DE),
            child:
                const Icon(Icons.wifi_tethering, size: 30, color: Colors.white),
          ),
        ),
      ),
    );
  }

  Widget buildPageWidget(int state) {
    Widget widget = const SizedBox();
    switch (state) {
      case 0:
        widget = const MainHomeView();
        break;
      case 1:
        widget = const MainCompassView();
        break;
    }
    return widget;
  }
}
