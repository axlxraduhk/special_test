import 'package:flutter/material.dart';

class MainHexagonalClipperWidget extends CustomClipper<Path> {
  MainHexagonalClipperWidget();

  @override
  Path getClip(Size size) {
    final double oneThirdWidth = size.width / 3.0;
    final Path path = Path()
      ..lineTo(0, size.height / 2)
      ..lineTo(size.width / 2, size.height)
      ..lineTo(size.width / 2, size.height)
      ..lineTo(size.width, size.height / 2)
      ..lineTo(oneThirdWidth * 2.5, 0)
      ..lineTo(oneThirdWidth / 2.5, 0)
      ..lineTo(0, size.height / 2)
      ..close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
