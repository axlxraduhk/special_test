import 'package:flutter/material.dart';
import 'package:special_test/common/widget/gradient_clip_widget.dart';

class MainBottomBarWidget extends StatelessWidget {
  const MainBottomBarWidget({Key? key, this.page, this.onSelect})
      : super(key: key);
  final int? page;
  final Function? onSelect;
  final Color selectedColor = Colors.white;
  final Color unSelectedColor = Colors.white54;

  @override
  Widget build(BuildContext context) => ClipPath(
        clipper: RPSCustomPainter(),
        child: GradientClipWidget(
          height: 110,
          width: MediaQuery.of(context).size.width,
          onPress: () {},
          onLongPress: () {},
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              const Spacer(flex: 5),
              InkWell(
                onTap: () {
                  onSelect!(0);
                },
                child: Icon(Icons.home,
                    size: 30,
                    color: page == 0 ? selectedColor : unSelectedColor),
              ),
              const Spacer(flex: 10),
              // const SizedBox(width: 16),
              Icon(Icons.search, size: 30, color: unSelectedColor),
              const Spacer(flex: 15),
              // const SizedBox(width: 16),
              InkWell(
                onTap: () {
                  onSelect!(1);
                },
                child: Icon(Icons.compass_calibration_outlined,
                    size: 30,
                    color: page == 1 ? selectedColor : unSelectedColor),
              ),
              const Spacer(flex: 10),
              // const SizedBox(width: 16),
              Icon(Icons.alarm, size: 30, color: unSelectedColor),
              const Spacer(flex: 5),
            ],
          ),
        ),
      );
}

class RPSCustomPainter extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final Path path_0 = Path();
    path_0.moveTo(size.width * 0.9396135, size.height * 1.187633);
    path_0.lineTo(size.width * 0.06038647, size.height * 1.187633);
    path_0.arcToPoint(Offset(size.width * 0.03688164, size.height * 1.174533),
        radius:
            Radius.elliptical(size.width * 0.06000483, size.height * 0.1656133),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(Offset(size.width * 0.01768599, size.height * 1.138813),
        radius:
            Radius.elliptical(size.width * 0.06018116, size.height * 0.1661000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(Offset(size.width * 0.004743961, size.height * 1.085833),
        radius:
            Radius.elliptical(size.width * 0.06018357, size.height * 0.1661067),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(Offset(size.width * 1.072679e-18, size.height * 1.020967),
        radius:
            Radius.elliptical(size.width * 0.06000966, size.height * 0.1656267),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 1.072679e-18, size.height * 0.1666600);
    path_0.arcToPoint(Offset(size.width * 0.004746377, size.height * 0.1017867),
        radius:
            Radius.elliptical(size.width * 0.06000725, size.height * 0.1656200),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(Offset(size.width * 0.01768841, size.height * 0.04881333),
        radius:
            Radius.elliptical(size.width * 0.06017391, size.height * 0.1660800),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(Offset(size.width * 0.03688406, size.height * 0.01309333),
        radius:
            Radius.elliptical(size.width * 0.06018357, size.height * 0.1661067),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(
        Offset(size.width * 0.06038647, size.height * 1.628327e-16),
        radius:
            Radius.elliptical(size.width * 0.06001449, size.height * 0.1656400),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.3937971, size.height * 1.628327e-16);
    path_0.cubicTo(
        size.width * 0.3989155,
        size.height * 0.01251333,
        size.width * 0.4044034,
        size.height * 0.02435333,
        size.width * 0.4066594,
        size.height * 0.02888667);
    path_0.lineTo(size.width * 0.4892150, size.height * 0.1946000);
    path_0.arcToPoint(Offset(size.width * 0.4934034, size.height * 0.1983600),
        radius: Radius.elliptical(
            size.width * 0.007079710, size.height * 0.01954000),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.arcToPoint(Offset(size.width * 0.4975845, size.height * 0.1946000),
        radius: Radius.elliptical(
            size.width * 0.007079710, size.height * 0.01954000),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.5801401, size.height * 0.02888667);
    path_0.cubicTo(
        size.width * 0.5824155,
        size.height * 0.02432000,
        size.width * 0.5881739,
        size.height * 0.01240667,
        size.width * 0.5936812,
        size.height * 1.776357e-16);
    path_0.lineTo(size.width * 0.9396135, size.height * 1.776357e-16);
    path_0.arcToPoint(Offset(size.width * 0.9631184, size.height * 0.01310000),
        radius:
            Radius.elliptical(size.width * 0.06001208, size.height * 0.1656333),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(Offset(size.width * 0.9823140, size.height * 0.04882000),
        radius:
            Radius.elliptical(size.width * 0.06018599, size.height * 0.1661133),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(Offset(size.width * 0.9952560, size.height * 0.1017933),
        radius:
            Radius.elliptical(size.width * 0.06017874, size.height * 0.1660933),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(Offset(size.width, size.height * 0.1666600),
        radius:
            Radius.elliptical(size.width * 0.06000242, size.height * 0.1656067),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width, size.height * 1.020967);
    path_0.arcToPoint(Offset(size.width * 0.9952536, size.height * 1.085840),
        radius:
            Radius.elliptical(size.width * 0.06000483, size.height * 0.1656133),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(Offset(size.width * 0.9823116, size.height * 1.138820),
        radius:
            Radius.elliptical(size.width * 0.06018357, size.height * 0.1661067),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(Offset(size.width * 0.9631159, size.height * 1.174540),
        radius:
            Radius.elliptical(size.width * 0.06018116, size.height * 0.1661000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.arcToPoint(Offset(size.width * 0.9396135, size.height * 1.187633),
        radius:
            Radius.elliptical(size.width * 0.06000242, size.height * 0.1656067),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.close();
    return path_0;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
