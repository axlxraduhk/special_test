import 'package:bloc/bloc.dart';
import 'package:special_test/introduction/model/introduction_model.dart';
import 'package:special_test/introduction/model/introduction_state.dart';

class IntroductionCubit extends Cubit<IntroductionState> {
  final IntroductionModel _introductionModel = IntroductionModel();

  IntroductionCubit()
      : super(IntroductionState(IntroductionModel().imageBeanList.first));

  void changedPage(int index) {
    emit(IntroductionState(_introductionModel.imageBeanList[index]));
  }
}
