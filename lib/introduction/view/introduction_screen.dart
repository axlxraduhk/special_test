import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:special_test/introduction/update/introduction_cubit.dart';
import 'package:special_test/introduction/view/introduction_view.dart';

class IntroductionScreen extends StatelessWidget {
  const IntroductionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (_) => IntroductionCubit(),
        child: IntroductionView(),
      );
}
