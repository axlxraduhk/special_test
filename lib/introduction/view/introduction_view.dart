import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:special_test/common/widget/background_widget.dart';
import 'package:special_test/introduction/update/introduction_cubit.dart';
import 'package:special_test/introduction/model/introduction_state.dart';
import 'package:special_test/main/view/main_screen.dart';

class IntroductionView extends StatelessWidget {
  IntroductionView({Key? key}) : super(key: key);

  final PageController controller = PageController();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return BlocBuilder<IntroductionCubit, IntroductionState>(
      builder: (context, state) => Scaffold(
        body: buildBodyWidget(size, state, context),
        floatingActionButton: FloatingActionButton(
            heroTag: 'introduction_floating_action_button_key',
            child: const Icon(Icons.arrow_forward, color: Colors.white),
            onPressed: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute<dynamic>(builder: (_) => const MainScreen()));
            }),
      ),
    );
  }

  Widget buildBodyWidget(
          Size size, IntroductionState state, BuildContext context) =>
      BackgroundWidget(
        child: Column(
          children: <Widget>[
            const Spacer(),
            SizedBox(
                width: size.width,
                height: size.width * 0.56,
                child: buildPageView(state, context, size)),
            Expanded(child: buildPageIndicatorTextColumn(state)),
          ],
        ),
      );

  PageView buildPageView(
          IntroductionState state, BuildContext context, Size size) =>
      PageView.builder(
        itemCount: state.bean.itemCount,
        controller: controller,
        onPageChanged: (index) =>
            context.read<IntroductionCubit>().changedPage(index),
        itemBuilder: (_, index) => Image.asset(state.bean.imagePath,
            width: size.width, fit: BoxFit.fill),
      );

  Column buildPageIndicatorTextColumn(IntroductionState state) => Column(
        children: <Widget>[
          const SizedBox(height: 16),
          SmoothPageIndicator(
              controller: controller,
              count: state.bean.itemCount,
              effect: const ExpandingDotsEffect(
                  activeDotColor: Colors.white, dotHeight: 8, dotWidth: 8)),
          const SizedBox(height: 32),
          Padding(
              padding: const EdgeInsets.all(0),
              child: Text(state.bean.title,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white))),
        ],
      );
}
