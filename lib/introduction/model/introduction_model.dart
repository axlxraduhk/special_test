class IntroductionModel {
  final List<IntroductionImageBean> imageBeanList = <IntroductionImageBean>[
    IntroductionImageBean(
        imagePath: 'assets/images/icon_introduction0.png',
        title: "Thunderbolt Fighting Magic",
        itemCount: 3),
    IntroductionImageBean(
        imagePath: 'assets/images/icon_introduction1.png',
        title: "Thunderbolt blue blood black yellow",
        itemCount: 3),
    IntroductionImageBean(
        imagePath: 'assets/images/icon_introduction2.png',
        title: "Thunderbolt Chao Lingque",
        itemCount: 3),
  ];
}

class IntroductionImageBean {
  IntroductionImageBean(
      {this.imagePath = '', this.title = '', this.itemCount = 0});

  String imagePath;
  String title;
  int itemCount;
}
