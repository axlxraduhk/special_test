import 'package:flutter/material.dart';

class ThemeColor {

  static Color primary = const Color(0xff450B91);
  
  static List<Color> gradient = <Color>[
    const Color(0xff192E5B),
    const Color(0xFF1D65A6),
    const Color(0xff192E5B),
  ];
}