import 'package:flutter/material.dart';
import 'package:special_test/common/theme/theme_color.dart';

class BackgroundWidget extends StatelessWidget {
  const BackgroundWidget({Key? key, this.child = const SizedBox()})
      : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: size.height,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: ThemeColor.gradient,
        ),
      ),
      child: child,
    );
  }
}
