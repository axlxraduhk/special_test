import 'package:flutter/material.dart';
import 'package:special_test/common/widget/gradient_clip_widget.dart';

class UserInformationWidget extends StatelessWidget {
  const UserInformationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Positioned(
      top: -50,
      child: GradientClipWidget(
        height: 180,
        width: size.width,
        opacity: 0.07,
        radius: 50,
        onPress: () {},
        onLongPress: () {},
        child: buildContainer(),
      ),
    );
  }

  Container buildContainer() {
    return Container(
      margin: const EdgeInsets.only(top: 80),
      padding: const EdgeInsets.only(top: 20, right: 20, left: 20, bottom: 10),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                buildNameRow(),
                buildChatCoinsRow(),
              ],
            ),
          ),
          const Spacer(),
          Container(
              width: 40,
              height: 5,
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.4),
                  borderRadius: BorderRadius.circular(20))),
        ],
      ),
    );
  }

  Row buildNameRow() {
    return Row(
      children: <Widget>[
        Image.asset("assets/images/icon_profile.png"),
        const SizedBox(width: 10),
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          const Text(
            "Nick",
            style: TextStyle(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
          ),
          Text("Online",
              style: TextStyle(
                  color: Colors.green[400],
                  fontSize: 11,
                  fontWeight: FontWeight.bold)),
        ]),
      ],
    );
  }

  Row buildChatCoinsRow() {
    return Row(
      children: <Widget>[
        Image.asset("assets/images/icon_chat.png"),
        const SizedBox(width: 20),
        GradientClipWidget(
          height: 35,
          width: 70,
          opacity: 0.05,
          radius: 50,
          onPress: () {},
          onLongPress: () {},
          child: Row(
            children: <Widget>[
              const SizedBox(width: 10),
              Image.asset("assets/images/icon_coins.png"),
              const SizedBox(width: 10),
              const Text("200",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ))
            ],
          ),
        ),
      ],
    );
  }
}
