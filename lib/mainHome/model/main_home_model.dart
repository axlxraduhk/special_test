class MainHomeModel {
  static final List<StreamerBean> streamerBeanList = <StreamerBean>[
    StreamerBean(
      name: "Biyin",
      game: "Minecraft",
      viewers: "150k",
      image: "icon_biyin.jpeg",
      tags: <String>["Acción", "Plataforma"],
    ),
    StreamerBean(
      name: "Auronpay",
      game: "Amoung us",
      viewers: "130k",
      image: "icon_auron.png",
      tags: <String>["Plataforma", "Deportes"],
    ),
    StreamerBean(
      name: "Rubius",
      game: "Amoung us",
      viewers: "180k",
      image: "icon_rubius.png",
      tags: <String>["Acción", "Plataforma", "Deportes"],
    ),
    StreamerBean(
      name: "Ibai",
      game: "Minecraft",
      viewers: "150k",
      image: "icon_ibai.jpeg",
      tags: <String>["Acción", "Deportes"],
    ),
  ];

  static final List<String> gamesList = <String>[
    "icon_maincra.png",
    "icon_cod.png",
    "icon_fail.png",
    "icon_pc.png",
  ];

  static final List<CategoryBean> categoryBeanList = <CategoryBean>[
    CategoryBean(image: "icon_accion.png", name: "Acción"),
    CategoryBean(image: "icon_sport.png", name: "Sport"),
    CategoryBean(image: "icon_rpg.png", name: "RPG"),
    CategoryBean(image: "icon_musica.png", name: "Música"),
  ];
}

class StreamerBean {
  StreamerBean({this.name, this.game, this.viewers, this.image, this.tags});

  String? name;
  String? game;
  String? viewers;
  String? image;
  List<String>? tags;
}

class CategoryBean {
  CategoryBean({this.name, this.image});

  String? name;
  String? image;
}
