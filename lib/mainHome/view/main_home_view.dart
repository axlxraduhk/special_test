import 'package:flutter/material.dart';
import 'package:special_test/mainHome/view/widget/main_home_card_image_widget.dart';
import 'package:special_test/mainHome/view/widget/main_home_category_card_widget.dart';
import 'package:special_test/mainHome/view/widget/main_home_games_widget.dart';
import 'package:special_test/mainHome/view/widget/main_home_segment_title_widget.dart';
import 'package:special_test/common/widget/user_information_widget.dart';

class MainHomeView extends StatelessWidget {
  const MainHomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Stack(
        children: <Widget>[
          const UserInformationWidget(),
          Positioned(
            top: 120,
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              padding: const EdgeInsets.all(15),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(height: 20),
                    MainHomeSegmentTitleWidget(page: 0, onTap: (v) {}),
                    const SizedBox(height: 20),
                    const MainHomeCategoryCardWidget(),
                    const SizedBox(height: 20),
                    buildText("Canales en vivo"),
                    const SizedBox(height: 20),
                    MainHomeCardImageWidget(),
                    buildText("Juegos recomendados"),
                    const SizedBox(height: 10),
                    const MainHomeGamesWidget(),
                    const SizedBox(height: 100),
                  ],
                ),
              ),
            ),
          ),
        ],
      );

  Text buildText(String text) {
    return Text(
      text,
      style: const TextStyle(
        color: Colors.white,
        fontSize: 18,
        fontWeight: FontWeight.w700,
      ),
    );
  }
}
