import 'package:flutter/material.dart';
import 'package:special_test/mainHome/model/main_home_model.dart';

class MainHomeGamesWidget extends StatelessWidget {
  const MainHomeGamesWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => SizedBox(
        width: double.infinity,
        height: 100,
        child: ListView.builder(
          itemCount: MainHomeModel.gamesList.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (_, index) => ClipRRect(
            borderRadius: const BorderRadius.all(
              Radius.circular(10),
            ),
            child: Container(
              margin: const EdgeInsets.all(10),
              child: Image.asset(
                "assets/images/${MainHomeModel.gamesList[index]}",
                width: 85,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      );
}
