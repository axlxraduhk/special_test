import 'package:flutter/material.dart';
import 'package:special_test/mainHome/model/main_home_model.dart';
import 'package:special_test/common/widget/gradient_clip_widget.dart';

class MainHomeCategoryCardWidget extends StatelessWidget {
  const MainHomeCategoryCardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return GradientClipWidget(
      height: 95,
      width: size.width,
      opacity: 0.07,
      radius: 15,
      onPress: () {},
      onLongPress: () {},
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 25, right: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: List<Widget>.generate(
                MainHomeModel.categoryBeanList.length,
                (index) => Column(
                  children: <Widget>[
                    Image.asset(
                        "assets/images/${MainHomeModel.categoryBeanList[index].image}"),
                    Text(
                      MainHomeModel.categoryBeanList[index].name!,
                      style: const TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            width: 40,
            height: 5,
            margin: const EdgeInsets.only(top: 5),
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.4),
                borderRadius: BorderRadius.circular(20)),
          ),
        ],
      ),
    );
  }
}
